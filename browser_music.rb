base_directory = "test/"

soundtrack_list = ""

Dir["#{base_directory}**/**"].each do |dir|
  # take the last file name of the file path
  file = dir.split("/")

  # split the last name and reject things, which has a . in their names (they are mp3 or pictures)
  album_name = file.last.split(".")
  if (album_name.size == 1)
    soundtrack_list << album_name.first + "\n"
  end
end

puts soundtrack_list

File.open("soundtrack.txt", "w") { |f| f.puts soundtrack_list}
