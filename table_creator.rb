# ruby table_creator.rb <absolute_path_to_images> <image_directory_name>

table_definition = "\t\t\t<td width=\"119\" height=\"42\" align=\"center\">"
table_row_start = "\t\t\t\t<tr>\n"
table_row_end = "\t\t\t\t</tr>\n"

base = ARGV[1]
prefix = "images/veranstaltungen"
text = <<-START
---
layout: default
title: #{base}
---
<div class="row area">
  <div class="span16 indent">
    <img class="center compressed" src="images/veranstaltungen/#{base}/000.png" title="Preise">
    <div class="clearer top-space"></div>
    <table>
START

images_name_array = []
Dir.glob(ARGV[0] + "*.jpg") do |image|
  images_name_array << image
end

images_name_array = images_name_array.sort

images_name_array.each do |picture|
  picture_name = File.basename(picture, ".jpg")
  picture = File.basename(picture)
  text << table_row_start if picture_name.to_i % 5 == 1

  text << table_definition
  text << "\n\t\t\t\t\t<a rel=\"fancy\" class=\"gallery\" href=\"#{prefix}/#{base}/#{picture}\" title=\"#{picture_name}\" ><img src=\"#{prefix}/#{base}/#{picture}\" width=\"72\" height=\"100\" alt=\"#{base} picture #{picture_name}\"></a></td>\n"

  text << table_row_end if File.basename(picture).to_i % 5 == 0
end

text += "\t\t\t\t</tr>\n" unless text.match /<\/tr>$/
text += <<-START
    </table>
  </div>
</div>
START

puts text
file = File.open("#{base}.html", 'w')
file.puts text
file.close

