## Contact

Feature request, bugs, questions, etc. can be send to <matthias.guenther@wikimatze.de>.


## License

This software is licensed under the [MIT license][mit].

© 2011-2013 Matthias Günther <matthias.guenther@wikimatze.de>.

[mit]: http://en.wikipedia.org/wiki/MIT_License
